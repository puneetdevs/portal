import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        private router: Router,
        private toastr: ToastrService,
        private spinner: NgxSpinnerService
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(evt => {
            }),
            catchError(err => {
                this.spinner.hide();
                console.log(err);
                if (err.status === 404) {
                    this.toastr.error(err.error.error ? err.error.error : err.error.errors);
                    this.router.navigate(['login']);
                } else if (err.status === 401) {
                    this.toastr.error('Unauthorised');
                    this.router.navigate(['login']);
                } else if (err.status !== 0) {
                    this.toastr.error(err.error.error);
                }
                return throwError(err);
            }));
    }
}
