import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req) {
            const currentUser = localStorage.getItem('currentUser');
            if (currentUser && localStorage.getItem('UserAccess')) {
                req = req.clone({
                    headers: new HttpHeaders({
                        Authorization: `Bearer ${currentUser}`,
                    })
                });
            }
        }
        return next.handle(req);
    }
}
