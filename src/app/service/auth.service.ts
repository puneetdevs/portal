import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUserSubject: any;
  private baseURL = 'https://portalapi.customerdemo.info/api';
    constructor(private http: HttpClient) { }

  login(email: any) {
    const data = {
      email
    };
    return this.http.post<any>(`${this.baseURL}/auth/login`, data)
    .pipe(
        map(repsonse => {
          return repsonse;
        }
      ));
  }
  authMe() {
    const data = '';
    return this.http.post<any>(`${this.baseURL}/auth/me`, data)
    .pipe(
        map(repsonse => {
          if (repsonse && repsonse.access_token) {
            this.set('currentUser', JSON.stringify(repsonse));
            this.currentUserSubject.next(repsonse);
          }
          return repsonse;
        }
      ));
  }
  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  setCurrentUser(data) {
      this.currentUserSubject.next(data);
  }

  set(key: string, value) {
    localStorage.setItem(key, value);
  }
  logout() {
    const data = '';
    return this.http.post<any>(`${this.baseURL}/auth/logout`, data)
    .pipe(
        map(repsonse => {
          return repsonse;
        }
      ));
  }
}
