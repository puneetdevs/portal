import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  isLoginForm = false;
  succesMessage = false;
  captchaString: string;
  isCaptchaField = false;
  captchaMessage = 'Enter the captcha';
  constructor(
    private formbuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private service: AuthService,
    private router: Router) {

  }
  ngOnInit() {
    this.isLoginForm = true;
    this.captchaString = '';
    this.form = this.formbuilder.group({
        email: ['', [Validators.required, Validators.email]],
        recaptchaReactive: ['', Validators.required ]
    });
    localStorage.clear();
    this.spinner.hide();
  }
  onEmail() {
    if (this.form.value.email !== '') {
      if (this.captchaString === '') {
        this.isCaptchaField = true;
        this.captchaMessage = 'Enter the captcha';
      } else {
        this.isCaptchaField = false;
        this.captchaMessage = '';
      }
    } else {
      this.isCaptchaField = false;
      this.captchaMessage = '';
    }
  }
  get email() {
    return this.form.get('email').setValidators(Validators.email);
  }

  get recaptchaReactive() {
    return this.form.get('recaptchaReactive');
  }

  onSubmit() {
    this.spinner.show();
    this.service.login(this.form.get('email').value).pipe().subscribe((response) => {
      this.spinner.hide();
      localStorage.clear();
      this.isLoginForm = false;
      this.succesMessage = true;
    });
  }

  resolved(captchaResponse: string) {
    this.captchaString = captchaResponse;
    this.isCaptchaField = false;
    if (this.captchaString) {
    this.captchaMessage = '';
    return this.form.get('recaptchaReactive').setValue(true);
   }
  }
}
