import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router) {

  }
  ngOnInit() {
    const token = this.route.snapshot.queryParamMap.get('token');
    if (token) {
      localStorage.setItem('UserAccess', 'True');
      localStorage.setItem('currentUser' , token);
      this.router.navigate(['dashboard']);
    } else {
      this.router.navigate(['login']);
      localStorage.clear();
    }
  }

}

