import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  token: any;
  name: any;
  email: any;
  isShow: boolean;
  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
     private router: Router,
      private service: AuthService) {

  }
  ngOnInit() {
    this.authmeNow();
  }
  authmeNow() {
    this.spinner.show();
    this.service.authMe().pipe().subscribe((response: any) => {
      if (response) {
        this.spinner.hide();
        this.isShow = true;
        this.name = response.name;
        this.email = response.email;
      }
    });
  }
  logout() {
    this.spinner.show();
    this.service.logout().pipe().subscribe((response: any) => {
      if (response) {
        this.spinner.hide();
        this.toastr.success(response.message);
        this.router.navigate(['login']);
      }
    });
  }
}
